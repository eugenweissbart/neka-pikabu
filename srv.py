from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    books_dir = os.path.join(os.getcwd(), 'static')
    books_list = [
        {'name': os.path.splitext(os.path.basename(x))[0].replace('_', ' '),
         'url' : '/static/{}'.format(os.path.basename(x))}
        for x in os.listdir(books_dir) if os.path.splitext(x)[1] == '.pdf']
    return render_template("index.html", books_list=books_list)

if __name__ == "__main__":
    app.run()
